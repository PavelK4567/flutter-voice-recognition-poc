import 'dart:convert';
import 'dart:io' as io;
import 'dart:math';
import 'package:dio/dio.dart';

import 'package:audio_recorder/audio_recorder.dart';
import 'package:file/file.dart';
import 'package:file/local.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

//void main() => runApp(new MyApp());
var dio = new Dio();
void main() async {
  runApp(new MyApp());
  String soapBaseUrl = "https://api.soapboxlabs.com/v1";
  String soapUrl = "words/analysis";
  String soapAuth = "authentication/client";
  String _appKey = "yhuI89FGH21sEfIeoQ4DXFyLgVxGcEyyLJRJjY3pEll7BkPQVqD2PS09POlkj34F";

  dio.options.baseUrl = soapBaseUrl;
  dio.options.headers["X-App-Key"] = _appKey;
 // Response _response;
//  _response = await dio.get(soapAuth + "?X-App-Key="+_appKey);
//  print("GET Response: " + _response.data.toString());
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();


}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('Voice recognition POC'),
        ),
        body: new AppBody(),
      ),
    );
  }
}

class AppBody extends StatefulWidget {
  final LocalFileSystem localFileSystem;

  AppBody({localFileSystem})
      : this.localFileSystem = localFileSystem ?? LocalFileSystem();

  @override
  State<StatefulWidget> createState() => new AppBodyState();
}

class AppBodyState extends State<AppBody> {
  Recording _recording = new Recording();
  bool _isRecording = false;
  Random random = new Random();
  TextEditingController _controller = new TextEditingController();





  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new Padding(
        padding: new EdgeInsets.all(8.0),
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              new FlatButton(
                onPressed: _isRecording ? null : _start,
                child: new Text("Start"),
                color: Colors.green,
              ),
              new FlatButton(
                onPressed: _isRecording ? _stop : null,
                child: new Text("Stop"),
                color: Colors.red,
              ),
              new TextField(
                controller: _controller,
                decoration: new InputDecoration(
                  hintText: 'Enter a custom path',
                ),
              ),
              new Text("File path of the record: ${_recording.path}"),
              new Text("Format: ${_recording.audioOutputFormat}"),
              new Text("Extension : ${_recording.extension}"),
              new Text(
                  "Audio recording duration : ${_recording.duration.toString()}")
            ]),
      ),
    );
  }

  _start() async {
    try {
      if (await AudioRecorder.hasPermissions) {
        if (_controller.text != null && _controller.text != "") {
          String path = _controller.text;
          if (!_controller.text.contains('/')) {
            io.Directory appDocDirectory =
            await getApplicationDocumentsDirectory();
            path = appDocDirectory.path + '/' + _controller.text;
          }
          print("Start recording: $path");
          await AudioRecorder.start(
              path: path, audioOutputFormat: AudioOutputFormat.AAC);
        } else {
          await AudioRecorder.start();
        }
        bool isRecording = await AudioRecorder.isRecording;
        setState(() {
          _recording = new Recording(duration: new Duration(), path: "");
          _isRecording = isRecording;
        });
      } else {
        Scaffold.of(context).showSnackBar(
            new SnackBar(content: new Text("You must accept permissions")));
      }
    } catch (e) {
      print(e);
    }
  }

  _stop() async {
    var recording = await AudioRecorder.stop();
    print("Stop recording: ${recording.path}");
    bool isRecording = await AudioRecorder.isRecording;
    File file = widget.localFileSystem.file(recording.path);
    print("  File length: ${await file.length()}");
    setState(() {
      _recording = recording;
      _isRecording = isRecording;
    });
    _controller.text = recording.path;
    startUpload(file);

  }

  startUpload(File file) async {
    File wavFile =
    //widget.localFileSystem.file("/storage/emulated/0/audio1.wav");
    file.renameSync("/storage/emulated/0/" + file.basename.replaceAll(".m4a", "") + ".wav");
    FormData formData = new FormData.from({
      "category": "orange",
      "user_token": "lamark111aaa",
      "file": new UploadFileInfo(wavFile, wavFile.basename),
    });
    print("sending: " + wavFile.basename);
    try{
      Response response = await dio.post("/speech/verification", data: formData);
      print(response.statusCode);
      print(response.data);
    } on DioError catch(e){
      print(e.message + e.response.toString());
    }


  }
}